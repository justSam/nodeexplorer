package main

import (
"flag"
"nodeExplorer/node"
"nodeExplorer/data"
)

func main() {
	peers := flag.Bool("peers", false, "Fetch peers")
	txs := flag.Bool("txs", false, "Fetch transactions")
	exportAS := flag.Bool("export-as", false, "Export AS data")
	exportVer := flag.Bool("export-ver", false, "Export Version data")
	exportTx := flag.Bool("export-tx", false, "Export transaction data")
	currency := flag.String("currency", "", "Currency to use (Values: BTC, ETH)")
	flag.Parse()

	if *peers && *currency == "ETH" {
		node.EthereumPeers()
	} else if *txs && *currency == "ETH" {
		node.EthereumTxs()
		//} else if *peers && *currency == "BTC" {
		//	node.BitcoinPeers()
		//} else if *txs && *currency == "BTC" {
		//	node.BitcoinTxs()
	} else if *exportAS {
		data.ExportASData(*currency)
	} else if *exportVer {
		data.ExportVersionData(*currency)
	} else if *exportTx {
		data.ExportTransactionData(*currency)
	} else {
		panic("No option passed")
	}
}