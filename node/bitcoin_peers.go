package node

import (
	"time"
	"fmt"

	"github.com/btcsuite/btcd/rpcclient"

	"nodeExplorer/core"
	"nodeExplorer/database"
	"nodeExplorer/core/normalize"
)

func BitcoinPeers() {
	config := core.LoadConfig("./bitcoin_config.toml")
	connCfg := &rpcclient.ConnConfig{
		Host:         config.ConnectRPC.Host,
		User:         config.ConnectRPC.RpcUser,
		Pass:         config.ConnectRPC.RpcPassword,
		HTTPPostMode: config.ConnectRPC.HTTPPostMode,
		DisableTLS:   config.ConnectRPC.DisableTLS,
	}
	println("Connecting to client...")
	client, err := rpcclient.New(connCfg, nil)
	core.CheckErr("Could not create rpc client: %v", err)
	defer client.Shutdown()

	database.Connect(config.Filepath)

	peerTicker := time.NewTicker(config.Node.Ticker * time.Millisecond)
	quit := make(chan struct{})
	for {
		select {
		case <-peerTicker.C:
			btcPeers, err := client.GetPeerInfo()
			core.CheckErr("Could not fetch peers: %v", err)

			timeObs := time.Now().Unix()
			peers := normalize.PeersFromBitcoin(btcPeers, timeObs, config.Node.ID)
			database.UpdatePeers(timeObs, config.Node.Currency, peers)

		case <-quit:
			peerTicker.Stop()
			fmt.Println("Tickers stopped")
			database.Disconnect()
			return
		}
	}
}
