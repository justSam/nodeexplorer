package node

import (
	"time"
	"fmt"
	_ "database/sql"

	"github.com/ethereum/go-ethereum/rpc"
	"github.com/ethereum/go-ethereum/p2p"

	"nodeExplorer/database"
	"nodeExplorer/core"
	"nodeExplorer/core/normalize"
)

func EthereumPeers() {
	config := core.LoadConfig("./ethereum_peer_config.toml")
	database.Connect(config.Filepath)
	fmt.Println(config.Node.Ticker)
	peerTicker := time.NewTicker(config.Node.Ticker * time.Millisecond)
	fmt.Println(peerTicker)
	quit := make(chan struct{})

	println("Connecting to client...")
	client, err := rpc.Dial(config.ConnectRPC.Host)
	core.CheckErr("Could not create rpc client: %v", err)

	for {
		select {
		case <-peerTicker.C:
			var ethPeers []*p2p.PeerInfo
			err = client.Call(&ethPeers, "admin_peers")
			core.CheckErr("Can't get list of peers:", err)
			timeObs := time.Now().Unix()

			peers := normalize.PeersFromEthereum(ethPeers, timeObs, config.Node.ID)
			database.UpdatePeers(timeObs, config.Node.Currency, peers)

		case <-quit:
			peerTicker.Stop()
			fmt.Println("Tickers stopped")
			database.Disconnect()
			return
		}
	}
}