package core

import (
	"github.com/BurntSushi/toml"
	"time"
)

type tomlConfig struct {
	Title 		string
	Filepath	string
	Database 	Database
	Node 		Node
	ConnectRPC	ConnectRPC
}

type Database struct {
	Server 		string
	User 		string
	Password	string
	DbName 		string
}

type Node struct {
	ID					int
	Ticker 				time.Duration
	Currency			string
}

type ConnectRPC struct {
	Host			string
	RpcUser			string
	RpcPassword		string
	HTTPPostMode		bool
	DisableTLS		bool
}

func LoadConfig(filename string) tomlConfig{
	var config tomlConfig
	if _, err := toml.DecodeFile(filename, &config); err != nil {
		panic(err)
	}
	config.Filepath = filename
	return config
}
