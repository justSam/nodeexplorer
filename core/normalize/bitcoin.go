package normalize

import (
	"net"
	"fmt"
	"strconv"
	"nodeExplorer/core"
	"nodeExplorer/database"
	"nodeExplorer/core/types"
	"github.com/btcsuite/btcd/btcjson"
	"github.com/btcsuite/btcd/chaincfg/chainhash"
)

func PeerFromBitcoin (btcPeer btcjson.GetPeerInfoResult, timeObs int64, nodeID int) types.Peer {
	remoteAddress, _, err := net.SplitHostPort(btcPeer.Addr)
	core.CheckErr("Failed to remove port: ", err)

	ASNumber := database.CheckExistingAS(remoteAddress)
	return types.Peer{fmt.Sprint(btcPeer.Version, ", ", btcPeer.SubVer), remoteAddress, ASNumber, timeObs, "BTC", nodeID}
}

func PeersFromBitcoin (btcPeers []btcjson.GetPeerInfoResult, timeObs int64, nodeID int) []types.Peer {
	var peers []types.Peer
	for _, btcPeer := range btcPeers {
		peers = append(peers, PeerFromBitcoin(btcPeer, timeObs, nodeID))
	}
	return peers
}


func TxFromBitcoin (btcTx *chainhash.Hash, status core.Status, timeObs int64, nodeID int) types.Transaction {
	return types.Transaction{btcTx.String(), timeObs, status, "BTC", nodeID}
}

func TxsFromBitcoin (btcTxs []*chainhash.Hash, timeObs int64, nodeID int) []types.Transaction {
	var transactions []types.Transaction
	for _, btcTx := range btcTxs {
		transactions = append(transactions, TxFromBitcoin(btcTx, core.Pending, timeObs, nodeID))
	}
	return transactions
}

func BlockFromBitcoin (btcBlock *btcjson.GetBlockVerboseResult) types.Block {
	return  types.Block{strconv.FormatInt(btcBlock.Height, 10), strconv.FormatInt(btcBlock.Time, 10), btcBlock.Tx}
}