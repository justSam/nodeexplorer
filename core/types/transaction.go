package types

import (
	"fmt"
	"nodeExplorer/core"
	"strconv"
)

type Receipt struct {
	Blocknumber string
	Status 		string
}

type Block struct {
	Number			string	`json:"number"`
	Timestamp		string	`json:"timestamp"`
	Transactions	[]string
}

func (block Block) GetNumber() int64 {
	number, err := strconv.ParseInt(block.Number, 0, 32)
	core.CheckErr("Failed to decode:", err)
	return number
}

type Transaction struct {
	Hash		  		string
	ObservedTime 		int64
	Status				core.Status
	Currency			string
	NodeID				int
}

func (tx Transaction) TxAsSqlValue() string {
	return fmt.Sprintf("('%s', '%d', '%s', '%s', '%d')", tx.Hash, tx.ObservedTime, tx.Status, tx.Currency, tx.NodeID)
}

type Transactions []Transaction

func (txs Transactions) GetHashes() []string {
	var hashes []string
	for _, tx := range txs {
		hashes = append(hashes, fmt.Sprintf("%s", tx.Hash))
	}
	return hashes
}
