package types

import (
	"fmt"
)

type Peer struct {
	Name		  		string
	RemoteAddress 		string
	ASNumber			string
	ConnectionTimestamp int64
	Currency			string
	NodeID				int
}

func (peer Peer) PeerAsSqlValue() string {
	return fmt.Sprintf("('%s', '%s', '%s', '%d', '%s', '%d')",
		peer.Name,
		peer.RemoteAddress,
		peer.ASNumber,
		peer.ConnectionTimestamp,
		peer.Currency,
		peer.NodeID)
}

type Peers []Peer

func (peers Peers) GetAddresses() []string {
	var addresses []string
	for _, peer := range peers {
		addresses = append(addresses, fmt.Sprintf("%s", peer.RemoteAddress))
	}
	return addresses
}

func (peers Peers) PeerAsSqlValues() []string {
	var peersSqlValues []string
	for _, peer := range peers {
		peersSqlValues = append(peersSqlValues, peer.PeerAsSqlValue())
	}
	return peersSqlValues
}
