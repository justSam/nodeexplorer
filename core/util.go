package core

import (
	"fmt"
	"net"
	"github.com/ammario/ipisp"
)

type Status string

const (
	Pending Status = "pending"
	Removed Status = "removed"
	Failed 	Status = "failed"
	Success Status = "success"
)

func LookupRemoteAddress (address string) string {

	client, err := ipisp.NewWhoisClient()
	CheckErr("Could not connect to Whois client:", err)
	IPInfo, err := client.LookupIP(net.ParseIP(address))
	CheckErr("Unable to parse IP:", err)

	ASNumber := fmt.Sprintf("%s", IPInfo.ASN)

	return ASNumber
}

func CheckErr(errorMessage string, err error) {
	if err != nil {
		fmt.Println(errorMessage, err)
		panic(err)
	}
}
